// Require model so we could use the model for searching
const User = require("../models/User.js");
const bcrypt = require("bcrypt")
const auth = require("../auth.js")

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email }).then(result => {
		// condition if there is an exsiting  user
		if(result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else
		{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    //Salt rounds
    // Salt rounds is proportional to hasshing rounds 
    // hash - asynchroursly
    //bcrypt - package for password hasing
    // hashScync - synchornously generate a hash
    password: bcrypt.hashSync(reqBody.password,10),
    mobileNo: reqBody.mobileNo
  })
  return newUser.save().then((user,error) =>{
    if (error){
      return false;
    }
    else{
      return true;
    }
  })
}
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}
module.exports.userDocument = () => {
  return User.findOne({}).then(result =>{
    return result;
  })
};
module.exports.getProfile = (reqBody) => {
  return User.findOne({_id:reqBody._id}).then(result =>{
		if(result == null){
			return false;
		}
    else{
      result.password = "*****";
      return result;
    }
  })   
 
};