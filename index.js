/*
GitBash:
npm init -y 
npm i express
npm i mongoose
npm i cors

*/

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute = require("./routes/userRoute.js");
// to create a express server/ application
const app = express();

// Middlewares = allows to bridge our backend application (server) to our front end 
// give access to front end
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);


//use to connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.2jzyijg.mongodb.net/CourseBooking",
  {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
// Prompts a message once connected

mongoose.connection.once('open', () => console.log("Now connected to Baal-Mongo DB Atlas."));


app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000 } `));