const mongoose = require("mongoose");


const courseSchema = new mongoose.Schema({
		name :{
			type: String,
      // Required
      // The "true" vallue defines if the field is required or not and the second element in the array is the message that will printed out in our terminal when the data is not present.
			required: [true, "Course is required"]
		},
		description :{
			type: String,
			required: [true, "description is required"]
		},
    price:{
      type: Number,
			required: [true, "price is required"]
    },
    isActive:{
      type: Boolean,
      default: true
    },
    
    createdOn:{
      type: Date,
      // The "new Date()" expression instantiates a new "date" that the current date and time whenever a coure is created in our database.
      default: new Date()
      },
        enrollees: [{

          userID: {String,
          required: [true, "userID is required"]
          },
          enrolledOne:{
            type: Date,
            default: new Date()
          }
        }
      
      ]
      

     
	})

  module.exports = mongoose.model("Task", courseSchema);