//authentication
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) =>{
  //payload
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  // call back function
  return jwt.sign(data, secret, {})
}